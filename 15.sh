#!/bin/bash

#escreva um script que execute o comando ping para 4 sites da internet e exiba na tela a quantidade que funcionou e a quantidade que falhou.

a="uol.com.br"
b="globo.com"
c="ifpb.edu.br"
d="bing.com"

total=0

for i in $a $b $c $d; do
	if ping -c 1 $i &> /dev/null; then
		total=$(( total + 1 ))
	fi
done 

echo $total
echo $(( 4 - $total ))
