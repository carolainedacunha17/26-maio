#!/bin/bash

#8.8.8.8 ---> $1
#127.0.0.1 -----> $2
#10.0.54.254 ----> $3
# $# = 3  -----> quantidade de linhas passadas.
# $* ----> todos
# $@ --->


#escreva um script shell q ping para todos os argumentos de linha de comando
total=0

for i in $*; do
	ping -c 1 $i
	total=$(( total + 1 )) 
done

echo $((4 - total))
