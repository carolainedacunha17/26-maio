#!/bin/bash

a=$1

#ver se o arquivo exite
if [ -e $a ]; then
	echo "ok"
else 
	echo "erro!"
fi

#menu
echo "digite 1 para as 10 primerias linhas"
echo "digite 2 para as 10 ultimas linhas"
echo "digite 3 para o numeros de linhas"
read op

#primeras 10
if [ $op = "1" ]; then
	head -10 $1  

#ultimas 10
elif [ $op = "2"  ]; then
	tail -10 $1

#numeros de linhas
elif [ $op = "3" ]; then
	wc -l $1

#caso seja digitado outra coisa
else
	echo "erro!"
fi

